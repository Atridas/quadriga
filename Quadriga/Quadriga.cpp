// Quadriga.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cassert>
#include <cstdio>

#include <algorithm>
#include <stack>
#include <vector>
#include <set>
#include <map>

#include <fstream>

#include <codecvt>

namespace Quadriga
{
	enum class Terminal
	{
		ERROR, END, IDENTIFICADOR, TERMINAL, OR, OPEN_PARENTHESIS, CLOSE_PARENTHESIS, NUMBER_SYMBOL, ARROBA, PLUS, ASTERISC, QUESTION, DASH, ASSIGN, SEMICOLON
	};

	struct TerminalData
	{
		Terminal type;
		const char32_t *textBeginning;
		int textSize;

		TerminalData() = default;
		TerminalData(Terminal _type) : type(_type), textBeginning(nullptr), textSize(0) {}
		TerminalData(Terminal _type, const char32_t *_textBeginning, int _textSize) : type(_type), textBeginning(_textBeginning), textSize(_textSize) {}
		TerminalData(Terminal _type, const char32_t *_textBeginning, const char32_t *_textEnd) : type(_type), textBeginning(_textBeginning), textSize(_textEnd - _textBeginning) {}
	};

	static bool IsASCIIDigit(char32_t character)
	{
		if (character >= U'0' && character <= U'9')
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	static bool IsUnicodeLetter(char32_t character)
	{
		if (
			(character >= U'a' && character <= U'z') || (character >= U'A' && character <= U'Z') ||						// Unicode Basic Latin
																														//		(character >= U'\u00C0' && character <= U'\u00FF' && character != U'\u00D7' && character != U'\u00F7') ||	// Unicode Latin-1
																														//		(character >= U'\u0100' && character <= U'\u017F') ||														// Unicode Latin Extended-A
																														//		(character >= U'\u0180' && character <= U'\u024F'))															// Unicode Latin Extended-B
			(character >= U'\u00C0' && character <= U'\u024F' && character != U'\u00D7' && character != U'\u00F7')
			)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	static bool IsUnicodeControlCharacter(char32_t character)
	{
		if (character <= U'\u001F' || character == U'\u00F7' || (character >= U'\u0080' && character <= U'\u009F'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	class Scanner
	{
	public:
		static const int LL_SIZE = 1;

		const char32_t *text;
		int currentTextIndex;
		TerminalData nextSymbols[LL_SIZE];
		int currentSavedSymbols;

		Scanner(const char32_t *_text) :text(_text), currentTextIndex(0), currentSavedSymbols(0) {}


		TerminalData PeekNextSymbol()
		{
			if (currentSavedSymbols == 0)
			{
				nextSymbols[0] = GetNextSymbol();
				++currentSavedSymbols;
			}
			return nextSymbols[0];
		}

		TerminalData GetNextSymbol()
		{
			if (currentSavedSymbols > 0)
			{
				TerminalData result = nextSymbols[0];
				--currentSavedSymbols;
				for (int i = 0; i < currentSavedSymbols; ++i)
					nextSymbols[i] = nextSymbols[i + 1];
				return result;
			}

			enum Status {
				BEGIN,
				COMMENT_1, COMMENT_2, COMMENT_3, COMMENT_4, COMMENT_5,
				NO_TERMINAL_1, NO_TERMINAL_2,
				TERMINAL_1, TERMINAL_2, TERMINAL_3,
				TERMINAL_UNICODE
			};

			Status status = BEGIN;
			int nestingCommentLevel = 0;

			const char32_t *textBeginning = &text[currentTextIndex];

			for (;;)
			{
				char32_t character = text[currentTextIndex];
				++currentTextIndex;

				switch (status)
				{
				case BEGIN:
					switch (character)
					{
					case U' ':
					case U'\n':
					case U'\r':
					case U'\t':
						textBeginning = &text[currentTextIndex];
						continue;
					case 0:
						return Terminal::END;
					case '#':
						return TerminalData(Terminal::NUMBER_SYMBOL, textBeginning, &text[currentTextIndex]);
					case '@':
						return TerminalData(Terminal::ARROBA, textBeginning, &text[currentTextIndex]);
					case '|':
						return TerminalData(Terminal::OR, textBeginning, &text[currentTextIndex]);
					case '(':
						return TerminalData(Terminal::OPEN_PARENTHESIS, textBeginning, &text[currentTextIndex]);
					case ')':
						return TerminalData(Terminal::CLOSE_PARENTHESIS, textBeginning, &text[currentTextIndex]);
					case '+':
						return TerminalData(Terminal::PLUS, textBeginning, &text[currentTextIndex]);
					case '*':
						return TerminalData(Terminal::ASTERISC, textBeginning, &text[currentTextIndex]);
					case '?':
						return TerminalData(Terminal::QUESTION, textBeginning, &text[currentTextIndex]);
					case '-':
						return TerminalData(Terminal::DASH, textBeginning, &text[currentTextIndex]);
					case '=':
						return TerminalData(Terminal::ASSIGN, textBeginning, &text[currentTextIndex]);
					case ';':
						return TerminalData(Terminal::SEMICOLON, textBeginning, &text[currentTextIndex]);
					case '_':
						status = NO_TERMINAL_2;
						continue;
					case '/':
						status = COMMENT_1;
						continue;
					case '\'':
						status = TERMINAL_1;
						continue;
					default:
						if (IsUnicodeLetter(character))
						{
							status = NO_TERMINAL_1;
							continue;
						}
						else
						{
							return Terminal::ERROR;
						}
					}

				case COMMENT_1: // first character were '/'
					if (character == U'/')
					{
						status = COMMENT_2;
						continue;
					}
					else if (character == U'*')
					{
						status = COMMENT_3;
						++nestingCommentLevel;
						continue;
					}
					else
					{
						return Terminal::ERROR;
					}

				case COMMENT_2: // first characters were '//'
					if (character == U'\n')
					{
						textBeginning = &text[currentTextIndex];
						status = BEGIN;
						continue;
					}
					else
					{
						continue;
					}

				case COMMENT_3: // first characters were '/*'
					if (character == U'*')
					{
						status = COMMENT_4;
						continue;
					}
					else if (character == U'/')
					{
						status = COMMENT_5;
						continue;
					}
					else
					{
						continue;
					}

				case COMMENT_4: // first characters were '/*...*'
					if (character == U'/')
					{
						--nestingCommentLevel;
						if (nestingCommentLevel == 0)
						{
							textBeginning = &text[currentTextIndex];
							status = BEGIN;
						}
						else
							status = COMMENT_3;
						continue;
					}
					else
					{
						status = COMMENT_3;
						continue;
					}

				case COMMENT_5: // first characters were '/*.../'
					if (character == U'*')
					{
						++nestingCommentLevel;
						status = COMMENT_3;
						continue;
					}
					else if (character == U'/')
					{
						status = COMMENT_5;
						continue;
					}
					else
					{
						status = COMMENT_3;
						continue;
					}

				case NO_TERMINAL_1: // first character was a letter or {'_'} + letter
					if (IsASCIIDigit(character) || IsUnicodeLetter(character) || character == U'_')
					{
						continue;
					}
					else
					{
						--currentTextIndex;
						return TerminalData(Terminal::IDENTIFICADOR, textBeginning, &text[currentTextIndex]);
					}

				case NO_TERMINAL_2: // firsts characters were '_'
					if (character == U'_')
					{
						status = NO_TERMINAL_2;
						continue;
					}
					else if (IsASCIIDigit(character) || IsUnicodeLetter(character))
					{
						status = NO_TERMINAL_1;
						continue;
					}
					else
					{
						return Terminal::ERROR;
					}

				case TERMINAL_1: // first character was '
					if (character == U'\\')
					{
						status = TERMINAL_3;
						continue;
					}
					else if (!IsUnicodeControlCharacter(character))
					{
						status = TERMINAL_2;
						continue;
					}
					else
					{
						return Terminal::ERROR;
					}
				case TERMINAL_2: // firsts characters were ' + something
					if (character == U'\'')
					{
						return TerminalData(Terminal::TERMINAL, textBeginning, &text[currentTextIndex]);
					}
					else
					{
						return Terminal::ERROR;
					}

				case TERMINAL_3: // firsts characters were "\ 
					switch (character)
					{
					case 'a':
					case 'b':
					case 'f':
					case 'n':
					case 'r':
					case 't':
					case 'v':
					case '\\':
					case '\'':
					case '"':
					case '?':
						status = TERMINAL_2;
						continue;
					case 'u':
					case 'U':
						status = TERMINAL_UNICODE;
						continue;
					default:
						return Terminal::ERROR;
					}
				case TERMINAL_UNICODE: // firsts characters were "\u 
					switch (character)
					{
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case 'a':
					case 'b':
					case 'c':
					case 'd':
					case 'e':
					case 'f':
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
						status = TERMINAL_UNICODE;
						continue;
					case '\'':
						return TerminalData(Terminal::TERMINAL, textBeginning, &text[currentTextIndex]);
					default:
						return Terminal::ERROR;
					}

				default:
					return Terminal::ERROR;
				}
			}
		}
	};

	enum class NoTerminal
	{
		Error, TerminalGroup, ConcatenationExp, ParenthesisExp, MultiplierExp, Exp, Def, DefList
	};

	enum class NoTerminalAuxiliar
	{
		TerminalGroup1, ConcatenationExp1, MultiplierExp1, Exp1, DefList1
	};

	enum class SymbolType
	{
		Terminal, NoTerminal, NoTerminalEnd, NoTerminalAuxiliar
	};

	struct Symbol
	{
		SymbolType type;
		union {
			Terminal terminal;
			NoTerminal noTerminal;
			NoTerminalAuxiliar auxiliar;
		};

		Symbol(TerminalData _terminal) : type(SymbolType::Terminal), terminal(_terminal.type) {}
		Symbol(NoTerminal _noTerminal) : type(SymbolType::NoTerminal), noTerminal(_noTerminal) {}
		Symbol(NoTerminalAuxiliar _auxiliar) : type(SymbolType::NoTerminalAuxiliar), auxiliar(_auxiliar) {}
		
		static Symbol NoTerminalEnd() {
			Symbol s;
			s.type = SymbolType::NoTerminalEnd;
			return s;
		}

	private:
		Symbol() = default;
	};

	std::vector<Symbol> Parse(Scanner& scanner)
	{
		std::vector<Symbol> result;
		std::stack<Symbol> stack;

		std::vector<TerminalData> nextTerminals;

		stack.push(Symbol(NoTerminal::DefList));

		for (;;)
		{
			if (nextTerminals.size() == 0)
			{
				nextTerminals.push_back(scanner.GetNextSymbol());
			}

			Symbol stackTop = stack.top();
			stack.pop();

			switch (stackTop.type)
			{
			case SymbolType::Terminal:
				if (stackTop.terminal == nextTerminals[0].type)
				{
					result.push_back(Symbol(nextTerminals[0].type)); // TODO additional data
					if (stackTop.terminal == Terminal::END)
					{
						return result;
					}
					else
					{
						nextTerminals.erase(nextTerminals.begin());
					}
					continue;
				}
				else
				{
					result.push_back(Symbol(Terminal::ERROR));
					return result;
				}
			case SymbolType::NoTerminal:

				result.push_back(stackTop.noTerminal);
				stack.push(Symbol::NoTerminalEnd());

				switch (stackTop.noTerminal)
				{
				case NoTerminal::TerminalGroup:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
						stack.push(Symbol(NoTerminalAuxiliar::TerminalGroup1));
						stack.push(Symbol(Terminal::TERMINAL));
						continue;
					case Terminal::NUMBER_SYMBOL:
						stack.push(Symbol(Terminal::NUMBER_SYMBOL));
						continue;
					case Terminal::ARROBA:
						stack.push(Symbol(Terminal::ARROBA));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::ConcatenationExp:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
						stack.push(Symbol(NoTerminalAuxiliar::ConcatenationExp1));
						stack.push(Symbol(NoTerminal::MultiplierExp));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::ParenthesisExp:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
						stack.push(Symbol(NoTerminal::TerminalGroup));
						continue;
					case Terminal::OPEN_PARENTHESIS:
						stack.push(Symbol(Terminal::CLOSE_PARENTHESIS));
						stack.push(Symbol(NoTerminal::Exp));
						stack.push(Symbol(Terminal::OPEN_PARENTHESIS));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::MultiplierExp:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
						stack.push(Symbol(NoTerminalAuxiliar::MultiplierExp1));
						stack.push(Symbol(NoTerminal::ParenthesisExp));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::Exp:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
						stack.push(Symbol(NoTerminalAuxiliar::Exp1));
						stack.push(Symbol(NoTerminal::ConcatenationExp));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::Def:
					switch (nextTerminals[0].type)
					{
					case Terminal::IDENTIFICADOR:
						stack.push(Symbol(Terminal::SEMICOLON));
						stack.push(Symbol(NoTerminal::Exp));
						stack.push(Symbol(Terminal::ASSIGN));
						stack.push(Symbol(Terminal::IDENTIFICADOR));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminal::DefList:
					switch (nextTerminals[0].type)
					{
					case Terminal::IDENTIFICADOR:
						stack.push(Symbol(NoTerminalAuxiliar::DefList1));
						stack.push(Symbol(NoTerminal::Def));
						continue;
					case Terminal::END:
						stack.push(Symbol(Terminal::END));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				default:
					result.push_back(Symbol(Terminal::ERROR));
					return result;
				}

			case SymbolType::NoTerminalAuxiliar:
				switch (stackTop.auxiliar)
				{
				case NoTerminalAuxiliar::TerminalGroup1:
					switch (nextTerminals[0].type)
					{
					case Terminal::DASH:
						stack.push(Symbol(Terminal::TERMINAL));
						stack.push(Symbol(Terminal::DASH));
						continue;
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
					case Terminal::CLOSE_PARENTHESIS:
					case Terminal::PLUS:
					case Terminal::ASTERISC:
					case Terminal::QUESTION:
					case Terminal::OR:
					case Terminal::SEMICOLON:
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminalAuxiliar::ConcatenationExp1:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
						stack.push(Symbol(NoTerminalAuxiliar::ConcatenationExp1));
						stack.push(Symbol(NoTerminal::MultiplierExp));
						continue;
					case Terminal::CLOSE_PARENTHESIS:
					case Terminal::OR:
					case Terminal::SEMICOLON:
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminalAuxiliar::MultiplierExp1:
					switch (nextTerminals[0].type)
					{
					case Terminal::TERMINAL:
					case Terminal::NUMBER_SYMBOL:
					case Terminal::ARROBA:
					case Terminal::OPEN_PARENTHESIS:
					case Terminal::CLOSE_PARENTHESIS:
					case Terminal::OR:
					case Terminal::SEMICOLON:
						continue;
					case Terminal::PLUS:
						stack.push(Symbol(Terminal::PLUS));
						continue;
					case Terminal::ASTERISC:
						stack.push(Symbol(Terminal::ASTERISC));
						continue;
					case Terminal::QUESTION:
						stack.push(Symbol(Terminal::QUESTION));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminalAuxiliar::Exp1:
					switch (nextTerminals[0].type)
					{
					case Terminal::OR:
						stack.push(Symbol(NoTerminalAuxiliar::Exp1));
						stack.push(Symbol(NoTerminal::ConcatenationExp));
						stack.push(Symbol(Terminal::OR));
						continue;
					case Terminal::SEMICOLON:
					case Terminal::CLOSE_PARENTHESIS:
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				case NoTerminalAuxiliar::DefList1:
					switch (nextTerminals[0].type)
					{
					case Terminal::IDENTIFICADOR:
						stack.push(Symbol(NoTerminalAuxiliar::DefList1));
						stack.push(Symbol(NoTerminal::Def));
						continue;
					case Terminal::END:
						stack.push(Symbol(Terminal::END));
						continue;
					default:
						result.push_back(Symbol(Terminal::ERROR));
						return result;
					}
				default:
					result.push_back(Symbol(Terminal::ERROR));
					return result;
				}
			case SymbolType::NoTerminalEnd:
				result.push_back(Symbol::NoTerminalEnd());
				continue;
			default:
				result.push_back(Symbol(Terminal::ERROR));
				return result;
			}

		}
	}

	struct AFND
	{
		int numStates;
		std::set<char32_t> terminals;
		std::set<int> endStates;
		std::map<int, std::map<char32_t, int>> transitions;
		std::map<int, std::set<int>> lambdaTransitions;

		void AddAFND(const AFND& other, std::set<int> toOrigin, std::set<int> fromEnd)
		{
			// copy transitions
			for (auto transitionNode : other.transitions)
			{
				for (auto transition : transitionNode.second)
				{
					transitions[transitionNode.first + numStates][transition.first] = transition.second + numStates;
				}
			}

			// copy lambda transitions
			for (auto transitionNode : other.lambdaTransitions)
			{
				for (auto transition : transitionNode.second)
				{
					lambdaTransitions[transitionNode.first + numStates].insert(transition + numStates);
				}
			}

			// copy terminals
			terminals.insert(other.terminals.begin(), other.terminals.end());


			// add transitions to origin
			for (auto transition : toOrigin)
			{
				lambdaTransitions[transition].insert(numStates);
			}

			// add transitions to fromTerminal
			for (auto transition : fromEnd)
			{
				for (auto end : other.endStates)
					lambdaTransitions[end + numStates].insert(transition);
			}

			numStates += other.numStates;
		}


		std::set<int> GetAllReachableStates(const std::set<int>& states) const
		{
			std::set<int> result;
			std::set<int> nextIteration = states;
			do
			{
				result = nextIteration;
				for (int innerState : result)
				{
					auto it = lambdaTransitions.find(innerState);
					if (it != lambdaTransitions.end())
					{
						for (int lambdaTransition : it->second)
						{
							nextIteration.insert(lambdaTransition);
						}
					}
				}
			} while (nextIteration.size() > result.size());
			return result;
		}

		std::set<int> GetAllReachableStates(int state) const
		{
			return GetAllReachableStates(std::set<int>({ state }));
		}
	};

	struct AFD
	{
		int numStates;
		int endStateModes;
		std::set<char32_t> terminals;
		std::map<int, int> endStates;
		std::map<int, std::map<char32_t, int>> transitions;

		AFD() : numStates(0), endStateModes(0) {}
		AFD(const AFND& afnd)
			: endStateModes(1)
			, terminals(afnd.terminals)
		{
			std::vector< std::set<int> > states;

			// compute initial state
			states.push_back(afnd.GetAllReachableStates(0));

			for (size_t i = 0; i < states.size(); ++i)
			{
				for (char32_t terminal : afnd.terminals)
				{
					std::set<int> stateDestiny;
					for (int subState : states[i])
					{
						auto it1 = afnd.transitions.find(subState);
						if (it1 != afnd.transitions.end())
						{
							auto it2 = it1->second.find(terminal);
							if (it2 != it1->second.end())
							{
								stateDestiny.insert(it2->second);
							}
						}
					}
					if (stateDestiny.size() > 0)
					{
						stateDestiny = afnd.GetAllReachableStates(stateDestiny);
						bool stateDestinyFound = false;
						for (size_t j = 0; j < states.size(); ++j)
						{
							if (states[j] == stateDestiny)
							{
								transitions[i][terminal] = j;
								stateDestinyFound = true;
							}
						}
						if (!stateDestinyFound)
						{
							transitions[i][terminal] = states.size();
							states.push_back(stateDestiny);
						}
					}
				}
			}

			for (size_t i = 0; i < states.size(); ++i)
			{
				for (int subState : states[i])
				{
					if (afnd.endStates.count(subState) > 0)
					{
						endStates[i] = 0;
						break;
					}
				}
			}

			numStates = states.size();

			RemoveNondistinguishableStates();
		}

		AFD(const AFD& first, const AFD& second)
			: endStateModes(first.endStateModes + second.endStateModes)
		{
			std::vector< std::set<int> > states;

			terminals = first.terminals;
			terminals.insert(second.terminals.begin(), second.terminals.end());

			int secondOffset = first.numStates;
			int secondEndStateModeOffset = first.endStateModes;

			// compute initial state
			{
				std::set<int> initialState;
				initialState.insert(0); // initial state of the first AFD
				initialState.insert(0 + secondOffset); // initial state of the second AFD

				states.push_back(initialState);
			}

			for (size_t i = 0; i < states.size(); ++i)
			{
				for (char32_t terminal : terminals)
				{
					std::set<int> stateDestiny;
					for (int subState : states[i])
					{
						if (subState < secondOffset)
						{
							auto it1 = first.transitions.find(subState);
							if (it1 != first.transitions.end())
							{
								auto it2 = it1->second.find(terminal);
								if (it2 != it1->second.end())
								{
									stateDestiny.insert(it2->second);
								}
							}
						}
						else
						{
							auto it1 = second.transitions.find(subState - secondOffset);
							if (it1 != second.transitions.end())
							{
								auto it2 = it1->second.find(terminal);
								if (it2 != it1->second.end())
								{
									stateDestiny.insert(it2->second + secondOffset);
								}
							}
						}
					}
					if (stateDestiny.size() > 0)
					{
						bool stateDestinyFound = false;
						for (size_t j = 0; j < states.size(); ++j)
						{
							if (states[j] == stateDestiny)
							{
								transitions[i][terminal] = j;
								stateDestinyFound = true;
							}
						}
						if (!stateDestinyFound)
						{
							transitions[i][terminal] = states.size();
							states.push_back(stateDestiny);
						}
					}
				}
			}

			for (size_t i = 0; i < states.size(); ++i)
			{
				for (int subState : states[i])
				{
					if (subState < secondOffset)
					{
						auto otherEndState = first.endStates.find(subState);
						if (otherEndState != first.endStates.end())
						{
							auto myEndState = endStates.find(i);
							if (myEndState == endStates.end() || myEndState->second > otherEndState->second)
							{
								endStates[i] = otherEndState->second;
							}
							break;
						}
					}
					else
					{

						auto otherEndState = second.endStates.find(subState - secondOffset);
						if (otherEndState != second.endStates.end())
						{
							auto myEndState = endStates.find(i);
							if (myEndState == endStates.end() || myEndState->second > (otherEndState->second + secondEndStateModeOffset))
							{
								endStates[i] = (otherEndState->second + secondEndStateModeOffset);
							}
							break;
						}
					}
				}
			}

			numStates = states.size();
		}

		static std::set<int> ComputeSetIntersection(const std::set<int>& A, const std::set<int>& B)
		{
			std::set<int> result;
			for (int a : A)
			{
				if (B.find(a) != B.end())
					result.insert(a);
			}
			return result;
		}
		static std::set<int> ComputeSetDifference(const std::set<int>& A, const std::set<int>& B)
		{
			std::set<int> result;
			for (int a : A)
			{
				if (B.find(a) == B.end())
					result.insert(a);
			}
			return result;
		}
		static void ComputeSetIntersectionAndDifference(const std::set<int>& A, const std::set<int>& B, std::set<int>& intersection_, std::set<int>& difference_)
		{
			intersection_.clear();
			difference_.clear();
			for (int a : A)
			{
				if (B.find(a) == B.end())
					difference_.insert(a);
				else
					intersection_.insert(a);
			}
		}

		// https://en.wikipedia.org/wiki/DFA_minimization#Hopcroft.27s_algorithm
		void RemoveNondistinguishableStates()
		{
			std::set< int > F, Q;
			std::map< int, std::set< int > > endStateGroups;
			for (auto endState : endStates)
			{
				F.insert(endState.first);
				endStateGroups[endState.second].insert(endState.first);
			}
			for (int i = 0; i < numStates; ++i)
				Q.insert(i);

			//std::set< std::set< int > > P = { F, ComputeSetDifference(Q, F) };
			//std::set< std::set< int > > W = { F };

			std::set< std::set< int > > W;

			for (auto endStateGroup : endStateGroups)
			{
				W.insert(endStateGroup.second);
			}
			std::set< std::set< int > > P = W;
			if(Q.size() > F.size())
				P.insert(ComputeSetDifference(Q, F));

			while (W.size() > 0)
			{
				auto w_it = W.begin();
				auto A = *w_it;
				W.erase(w_it);
				for (char32_t c : terminals)
				{
					std::set< int > X;
					for (int origin = 0; origin < numStates; ++origin)
					{
						auto t_it = transitions[origin].find(c);
						if (t_it != transitions[origin].end())
						{
							int dest = t_it->second;
							if (A.count(dest) > 0)
							{
								X.insert(origin);
							}
						}
					}
					std::set< std::set< int > > nextP;
					for (auto Y : P)
					{
						std::set<int> XiY, Y_X;
						ComputeSetIntersectionAndDifference(Y, X, XiY, Y_X);
						if (XiY.size() > 0 && Y_X.size() > 0)
						{
							nextP.insert(XiY);
							nextP.insert(Y_X);
							auto w_it2 = W.find(Y);
							if (w_it2 != W.end())
							{
								W.erase(w_it2);
								W.insert(XiY);
								W.insert(Y_X);
							}
							else if (XiY.size() <= Y_X.size())
							{
								W.insert(XiY);
							}
							else
							{
								W.insert(Y_X);
							}
						}
						else
						{
							nextP.insert(Y);
						}
					}
					P = nextP;
				}
			}

			std::map<int, int> oldToNewStates;
			std::map<int, int> newEndStates;
			int newNumStates = 0;
			{
				bool initialStateFound = false;
				for (auto p : P)
				{
					if (p.size() > 0)
					{
						int thisState;
						if (initialStateFound)
						{
							thisState = newNumStates++;
						}
						else if (p.count(0) > 0)
						{
							thisState = 0;
							++newNumStates;
							initialStateFound = true;
						}
						else
						{
							thisState = ++newNumStates;
						}
						int endStateLabel = -1;
						for (int s : p)
						{
							assert(oldToNewStates.find(s) == oldToNewStates.end());
							oldToNewStates[s] = thisState;
							auto es_it = endStates.find(s);
							if (es_it != endStates.end())
							{
								assert(endStateLabel < 0 || endStateLabel == es_it->second);
								endStateLabel = es_it->second;
							}
						}
						if (endStateLabel >= 0)
						{
							newEndStates[thisState] = endStateLabel;
						}
					}
				}
			}
			std::map<int, std::map<char32_t, int>> newTransitions;
			for (auto t_it : transitions)
			{
				int origin = t_it.first;
				assert(oldToNewStates.find(origin) != oldToNewStates.end());
				int newOrigin = oldToNewStates[origin];
				for (auto t_it2 : t_it.second)
				{
					char32_t c = t_it2.first;
					int dest = t_it2.second;
					assert(oldToNewStates.find(dest) != oldToNewStates.end());
					int newDest = oldToNewStates[dest];

					assert(newTransitions.find(newOrigin) == newTransitions.end() ||
						newTransitions[newOrigin].find(c) == newTransitions[newOrigin].end() ||
						newTransitions[newOrigin][c] == newDest);

					newTransitions[newOrigin][c] = newDest;
				}
			}


			endStates = newEndStates;
			numStates = newNumStates;
			transitions = newTransitions;
		}
	};

	class Parser
	{
	public:

		AFND automat;

		AFD finalAutomat;
		std::vector<std::u32string> labels;

		void ParseDefList(Scanner &scanner)
		{
			for (bool endRepeat = false; !endRepeat;) 
			{
				switch (scanner.PeekNextSymbol().type)
				{
				case Terminal::IDENTIFICADOR:
					ParseDef(scanner);
					finalAutomat = AFD(finalAutomat, automat);
					break;
				default:
					endRepeat = true;
					break;
				}
			}

			finalAutomat.RemoveNondistinguishableStates();
		}

		void ParseDef(Scanner &scanner)
		{
			auto id = scanner.GetNextSymbol();
			assert(id.type == Terminal::IDENTIFICADOR);

			auto as = scanner.GetNextSymbol();
			assert(as.type == Terminal::ASSIGN);

			ParseExp(scanner);

			{
				std::u32string text;
				for (int i = 0; i < id.textSize; ++i)
					text += id.textBeginning[i];
				labels.push_back(text);
			}

			auto sc = scanner.GetNextSymbol();
			assert(sc.type == Terminal::SEMICOLON);
		}

		void ParseExp(Scanner &scanner)
		{
			ParseConcatenationExp(scanner);

			AFND oldAutomat = automat;

			for (bool endRepeat = false; !endRepeat;)
			{
				switch (scanner.PeekNextSymbol().type)
				{
				case Terminal::OR:
				{
					auto or = scanner.GetNextSymbol();
					assert(or .type == Terminal::OR);

					ParseConcatenationExp(scanner);
					{
						AFND newAutomat;
						newAutomat.numStates = 2;
						newAutomat.endStates.insert(1);

						std::set<int> toOrigin = { 0 };
						std::set<int> fromEnd = { 1 };
						newAutomat.AddAFND(oldAutomat, toOrigin, fromEnd);
						newAutomat.AddAFND(automat, toOrigin, fromEnd);

						automat = oldAutomat = newAutomat;
					}
					break;
				}
				default:
					endRepeat = true;
					break;
				}
			}
		}

		void ParseConcatenationExp(Scanner &scanner)
		{
			ParseMultiplierExp(scanner);

			AFND oldAutomat = automat;

			for (bool endRepeat = false; !endRepeat;)
			{
				switch (scanner.PeekNextSymbol().type)
				{
				case Terminal::TERMINAL:
				case Terminal::NUMBER_SYMBOL:
				case Terminal::ARROBA:
				case Terminal::OPEN_PARENTHESIS:
					ParseMultiplierExp(scanner);

					{
						AFND newAutomat;
						newAutomat.numStates = 3;
						newAutomat.endStates.insert(2);
						{
							std::set<int> toOrigin = { 0 };
							std::set<int> fromEnd = { 1 };
							newAutomat.AddAFND(oldAutomat, toOrigin, fromEnd);
						}
						{
							std::set<int> toOrigin = { 1 };
							std::set<int> fromEnd = { 2 };
							newAutomat.AddAFND(automat, toOrigin, fromEnd);
						}

						automat = oldAutomat = newAutomat;
					}

					break;
				default:
					endRepeat = true;
					break;
				}
			}
		}

		void ParseMultiplierExp(Scanner &scanner)
		{
			ParseParenthesisExp(scanner);

			switch (scanner.PeekNextSymbol().type)
			{
			case Terminal::PLUS:
			case Terminal::ASTERISC:
			case Terminal::QUESTION:
				switch (scanner.PeekNextSymbol().type)
				{
				case Terminal::PLUS:
				{
					auto pl = scanner.GetNextSymbol();
					assert(pl.type == Terminal::PLUS);

					{
						AFND newAutomat;
						newAutomat.numStates = 2;
						newAutomat.endStates.insert(1);
						std::set<int> toOrigin = { 0, 1 };
						std::set<int> fromEnd = { 1 };
						newAutomat.AddAFND(automat, toOrigin, fromEnd);

						automat = newAutomat;
					}

					break;
				}
				case Terminal::ASTERISC:
				{
					auto as = scanner.GetNextSymbol();
					assert(as.type == Terminal::ASTERISC);

					{
						AFND newAutomat;
						newAutomat.numStates = 2;
						newAutomat.endStates.insert(1);
						newAutomat.lambdaTransitions[0].insert(1);
						std::set<int> toOrigin = { 0, 1 };
						std::set<int> fromEnd = { 1 };
						newAutomat.AddAFND(automat, toOrigin, fromEnd);

						automat = newAutomat;
					}

					break;
				}
				case Terminal::QUESTION:
				{
					auto as = scanner.GetNextSymbol();
					assert(as.type == Terminal::QUESTION);

					{
						AFND newAutomat;
						newAutomat.numStates = 2;
						newAutomat.endStates.insert(1);
						newAutomat.lambdaTransitions[0].insert(1);
						std::set<int> toOrigin = { 0 };
						std::set<int> fromEnd = { 1 };
						newAutomat.AddAFND(automat, toOrigin, fromEnd);

						automat = newAutomat;
					}

					break;
				}
				}
				break;
			}
		}

		void ParseParenthesisExp(Scanner &scanner)
		{
			switch (scanner.PeekNextSymbol().type)
			{
			case Terminal::TERMINAL:
			case Terminal::NUMBER_SYMBOL:
			case Terminal::ARROBA:
				ParseTerminalGroup(scanner);
				break;
			case Terminal::OPEN_PARENTHESIS:
				auto op = scanner.GetNextSymbol();
				assert(op.type == Terminal::OPEN_PARENTHESIS);

				ParseExp(scanner);

				auto cp = scanner.GetNextSymbol();
				assert(cp.type == Terminal::CLOSE_PARENTHESIS);
				break;
			}
		}

		void ParseTerminalGroup(Scanner &scanner)
		{
			switch (scanner.PeekNextSymbol().type)
			{
			case Terminal::TERMINAL:
			{
				auto tr = scanner.GetNextSymbol();
				assert(tr.type == Terminal::TERMINAL);

				{
					AFND localAutomat;
					char32_t t = TerminalToChar(tr);
					localAutomat.numStates = 2;
					localAutomat.terminals.insert(t);
					localAutomat.transitions[0][t] = 1;
					localAutomat.endStates.insert(1);

					automat = localAutomat;
				}

				switch (scanner.PeekNextSymbol().type)
				{
				case Terminal::DASH:
					auto da = scanner.GetNextSymbol();
					assert(da.type == Terminal::DASH);

					auto tr2 = scanner.GetNextSymbol();
					assert(tr2.type == Terminal::TERMINAL);

					{
						char32_t t1 = TerminalToChar(tr);
						char32_t t2 = TerminalToChar(tr2);
						assert(t1 <= t2);
						for (char32_t t = t1 + 1; t <= t2; ++t)
						{
							automat.terminals.insert(t);
							automat.transitions[0][t] = 1;
						}
					}
					break;
				}

				break;
			}
			case Terminal::NUMBER_SYMBOL:
			{
				auto ns = scanner.GetNextSymbol();
				assert(ns.type == Terminal::NUMBER_SYMBOL);
				{
					AFND localAutomat;
					localAutomat.numStates = 2;
					for (char32_t t = '0'; t <= '9'; ++t)
					{
						localAutomat.terminals.insert(t);
						localAutomat.transitions[0][t] = 1;
					}
					localAutomat.endStates.insert(1);

					automat = localAutomat;
				}
				break;
			}
			case Terminal::ARROBA:
			{
				auto ar = scanner.GetNextSymbol();
				assert(ar.type == Terminal::ARROBA);

				{
					AFND localAutomat;
					localAutomat.numStates = 2;
					for (char32_t t = 'a'; t <= 'z'; ++t)
					{
						localAutomat.terminals.insert(t);
						localAutomat.transitions[0][t] = 1;
					}
					for (char32_t t = 'A'; t <= 'Z'; ++t)
					{
						localAutomat.terminals.insert(t);
						localAutomat.transitions[0][t] = 1;
					}

					for (char32_t t = U'\u00C0'; t <= U'\u024F'; ++t)
						if (t != U'\u00D7' && t != U'\u00F7')
						{
							localAutomat.terminals.insert(t);
							localAutomat.transitions[0][t] = 1;
						}

					localAutomat.endStates.insert(1);

					automat = localAutomat;
					
				}
				break;
			}
			}
		}

		char32_t TerminalToChar(TerminalData terminal)
		{
			assert(terminal.type == Terminal::TERMINAL);
			assert(terminal.textSize >= 3);
			if (terminal.textBeginning[1] == '\\')
			{
				switch (terminal.textBeginning[2])
				{
				case 'a':
					return '\a';
				case 'b':
					return '\b';
				case 'f':
					return '\f';
				case 'n':
					return '\n';
				case 'r':
					return '\r';
				case 't':
					return '\t';
				case 'v':
					return '\v';
				case '\\':
					return '\\';
				case '\'':
					return '\'';
				case '"':
					return '\"';
				case '?':
					return '\?';
				case 'u':
				case 'U':
				{
					char32_t result = 0;
					for (int i = 3; i < terminal.textSize - 1; ++i)
					{
						result *= 16;
						char32_t digit = terminal.textBeginning[i];
						if (digit >= '0' && digit <= '9')
							result += digit - '0';
						else if (digit >= 'a' && digit <= 'f')
							result += 10 + digit - 'a';
						else if (digit >= 'A' && digit <= 'F')
							result += 10 + digit - 'A';
						else
							assert(false);
					}

					return result;
				}
				default:
					assert(false); // TODO implement
					break;
				}
				return 0;
			}
			else
			{
				assert(terminal.textSize == 3);
				return terminal.textBeginning[1];
			}
		}
	};

	std::u32string ToU32(int i)
	{
		std::u32string stateNumber;
		int s_aux = i;
		while (s_aux > 0)
		{
			stateNumber = (char32_t)(U'0' + (s_aux % 10)) + stateNumber;
			s_aux = s_aux / 10;
		}
		if (stateNumber.length() > 0)
			return stateNumber;
		else
			return U"0";
	}

	std::u32string ToLiteral(char32_t c)
	{
		if (c >= 0x20 && c <= 0x7E)
		{
			if (c == U'\\')
			{
				return std::u32string(U"U'\\\\'");
			}
			else if(c == U'\'')
			{
				return std::u32string(U"U'\\''");
			}
			else if(c == U'\"')
			{
				return std::u32string(U"U'\\\"'");
			}
			else
			{
				return std::u32string(U"U'") + c + std::u32string(U"'");
			}
		}
		else
		{
			return ToU32(c);
		}
	}

	// To Code
	std::u32string ToCode(const AFD& automat, const std::vector<std::u32string>& labels)
	{
		std::u32string result;


		result += U"enum class Terminal\n";
		result += U"{\n";
		result += U"    ERROR,\n";
		result += U"    END,\n";
		for (const std::u32string& value : labels)
		{
			if (value == std::u32string(U"QUADRIGA_IGNORE")); // skip
			else if (value == std::u32string(U"QUADRIGA_BEGIN_COMMENT")); // skip
			else
				result += U"    " + value + U",\n";
		}
		result += U"};\n";

		result += U"struct TerminalData\n";
		result += U"{\n";
		result += U"    Terminal type;\n";
		result += U"    const char32_t *textBeginning;\n";
		result += U"    int textSize;\n";
		result += U"    \n";
		result += U"    TerminalData() = default;\n";
		result += U"    TerminalData(Terminal _type) : type(_type), textBeginning(nullptr), textSize(0) {}\n";
		result += U"    TerminalData(Terminal _type, const char32_t *_textBeginning, int _textSize) : type(_type), textBeginning(_textBeginning), textSize(_textSize) {}\n";
		result += U"    TerminalData(Terminal _type, const char32_t *_textBeginning, const char32_t *_textEnd) : type(_type), textBeginning(_textBeginning), textSize((int)(_textEnd - _textBeginning)) {}\n";
		result += U"};\n";


		result += U"\n";
		result += U"\n";

		result += U"class Scanner\n";
		result += U"{\n";
		result += U"public:\n";
		result += U"    static const int LL_SIZE = 1;\n";
		result += U"\n";
		result += U"    const char32_t *text;\n";
		result += U"    int currentTextIndex;\n";
		result += U"    TerminalData nextSymbols[LL_SIZE];\n";
		result += U"    int currentSavedSymbols;\n";

		result += U"\n";
		result += U"\n";

		result += U"    Scanner(const char32_t *_text) :text(_text), currentTextIndex(0), currentSavedSymbols(0) {}\n";

		result += U"\n";
		result += U"\n";

		result += U"    TerminalData PeekNextSymbol()\n";
		result += U"    {\n";
		result += U"        if (currentSavedSymbols == 0)\n";
		result += U"        {\n";
		result += U"            nextSymbols[0] = GetNextSymbol();\n";
		result += U"            ++currentSavedSymbols;\n";
		result += U"        }\n";
		result += U"        return nextSymbols[0];\n";
		result += U"    }\n";

		result += U"\n";
		result += U"\n";

		result += U"    TerminalData GetNextSymbol()\n";
		result += U"    {\n";
		result += U"        if (currentSavedSymbols > 0)\n";
		result += U"        {\n";
		result += U"            TerminalData result = nextSymbols[0];\n";
		result += U"            --currentSavedSymbols;\n";
		result += U"            for (int i = 0; i < currentSavedSymbols; ++i)\n";
		result += U"                nextSymbols[i] = nextSymbols[i + 1];\n";
		result += U"            return result;\n";
		result += U"        }\n";

		result += U"\n";
		result += U"\n";

		result += U"        int state = 0;\n";
		result += U"        const char32_t *textBeginning = &text[currentTextIndex];\n";

		result += U"\n";
		result += U"\n";

		result += U"        for (;;)\n";
		result += U"        {\n";
		result += U"            char32_t character = text[currentTextIndex];\n";
		result += U"            ++currentTextIndex;\n";
		result += U"            \n";
		result += U"            switch (state)\n";
		result += U"            {\n";

		for (int s = 0; s < automat.numStates; ++s)
		{
			{
				result += U"                case " + ToU32(s) + U":\n";
			}

			bool innerSwitchAdded = false;

			if (s == 0)
			{
				result += U"                    switch (character)\n";
				result += U"                    {\n";
				innerSwitchAdded = true;
				// -------------- end stream
				result += U"                    case 0:\n";
				result += U"                        return Terminal::END;\n";
				/*
				// -------------- skip characters
				result += U"                    case U' ':\n";
				result += U"                    case U'\\n':\n";
				result += U"                    case U'\\r':\n";
				result += U"                    case U'\\t':\n";
				result += U"                        continue;\n";
				*/
			}

			std::map< int, std::set<char32_t> > transitionGroups;
			auto t_it = automat.transitions.find(s);
			if (t_it != automat.transitions.end())
			{
				for (auto transition : t_it->second)
				{
					transitionGroups[transition.second].insert(transition.first);
					/*result += U"                    case " + ToLiteral(transition.first); + U":\n";


					result += U"                        break;\n";*/
				}
			}

			std::u32string defaultText;

			for (auto transitionGroup : transitionGroups)
			{
				bool toDefault = false;
				std::u32string tabs;
				if(transitionGroup.second.size() <= 5)
				{
					if (!innerSwitchAdded)
					{
						result += U"                    switch (character)\n";
						result += U"                    {\n";
						innerSwitchAdded = true;
					}
					assert(transitionGroup.second.size() > 0);
					tabs = U"                    ";
					for(auto c : transitionGroup.second)
						result += tabs + U"case " + ToLiteral(c) + U":\n";
				}
				else
				{
					defaultText += tabs = U"                        ";

					if (toDefault) // ja tenim minim 1 cas
						defaultText += U"else ";

					defaultText += U"if ( ";

					char32_t lastCharacter = 0xffffffff;
					for (auto c : transitionGroup.second)
					{
						if (lastCharacter == 0xffffffff)
						{
							defaultText += U"( character >= " + ToLiteral(c) + U" && ";
						}
						else if (c != lastCharacter + 1)
						{
							if (c == lastCharacter + 2)
							{
								defaultText += U"character != " + ToLiteral(c - 1) + U" && ";
							}
							else
							{
								defaultText += U"character <= " + ToLiteral(lastCharacter) + U" ) || ( character >= " + ToLiteral(c) + U" && ";
							}
						}
						lastCharacter = c;
					}

					defaultText += U"character <= " + ToLiteral(lastCharacter) + U" ) )\n";

					defaultText += tabs + U"{\n";
					toDefault = true;
				}

				std::u32string innerText;
				if(transitionGroup.first == s)
					innerText = tabs + U"    // state = " + ToU32(transitionGroup.first) + U";\n";
				else
					innerText = tabs + U"    state = " + ToU32(transitionGroup.first) + U";\n";

				if (!toDefault)
				{
					result += innerText + tabs + U"    break;\n";
				}
				else
				{
					defaultText += innerText + tabs + U"}\n";
				}
			}

			if (innerSwitchAdded)
				result += U"                    default:\n";

			result += defaultText;
			if (defaultText.length() > 0)
			{
				result += U"                        else\n";
			}
			result += U"                        {\n";
			auto es_it = automat.endStates.find(s);
			if (es_it == automat.endStates.end())
			{
				result += U"                            return Terminal::ERROR;\n";
			}
			else if (labels[es_it->second] == std::u32string(U"QUADRIGA_IGNORE"))
			{
				result += U"                            --currentTextIndex;\n";
				result += U"                            textBeginning = &text[currentTextIndex];\n";
				result += U"                            state = 0;\n";
			}
			else if (labels[es_it->second] == std::u32string(U"QUADRIGA_BEGIN_COMMENT"))
			{
				result += U"                            while(text[currentTextIndex] != '\\n' && text[currentTextIndex] != '\\r' && text[currentTextIndex] != '\\0')\n";
				result += U"                                ++currentTextIndex;\n";
				result += U"                            textBeginning = &text[currentTextIndex];\n";
				result += U"                            state = 0;\n";
			}
			else
			{
				result += U"                            --currentTextIndex;\n";
				result += U"                            return TerminalData(Terminal::" + labels[es_it->second] + U", textBeginning, &text[currentTextIndex]);\n";
			}
			result += U"                        }\n";

			if (innerSwitchAdded)
			{
				result += U"                        break;\n";
				result += U"                    }\n";
			}
			result += U"                    break;\n";
		}

		// --------------- 
		result += U"            default:\n";
		result += U"                return Terminal::ERROR;\n";
		result += U"            }\n";
		result += U"        }\n";
		result += U"    }\n";
		result += U"};\n";

		return result;
	};
}

// tests

void PrintSymbolSequence(char32_t *is)
{
	using namespace Quadriga;
	Quadriga::Scanner scanner(is);

	printf("-- Scanner : \n");

	for (;;)
	{
		Terminal symbol = scanner.GetNextSymbol().type;
		switch (symbol)
		{
		case Quadriga::Terminal::ERROR:
			printf("Error!!\n");
			return;
		case Quadriga::Terminal::END:
			printf("End\n");
			goto END_FOR;
		case Quadriga::Terminal::TERMINAL:
			printf(" TERMINAL ");
			continue;
		case Quadriga::Terminal::IDENTIFICADOR:
			printf(" IDENTIFICADOR ");
			continue;
		case Quadriga::Terminal::PLUS:
			printf(" + ");
			continue;
		case Quadriga::Terminal::ASTERISC:
			printf(" * ");
			continue;
		case Quadriga::Terminal::QUESTION:
			printf(" ? ");
			continue;
		case Quadriga::Terminal::ARROBA:
			printf(" @ ");
			continue;
		case Quadriga::Terminal::NUMBER_SYMBOL:
			printf(" # ");
			continue;
		case Quadriga::Terminal::OR:
			printf(" | ");
			continue;
		case Quadriga::Terminal::DASH:
			printf(" - ");
			continue;
		case Quadriga::Terminal::OPEN_PARENTHESIS:
			printf(" ( ");
			continue;
		case Quadriga::Terminal::CLOSE_PARENTHESIS:
			printf(" ) ");
			continue;
		case Quadriga::Terminal::ASSIGN:
			printf(" = ");
			continue;
		case Quadriga::Terminal::SEMICOLON:
			printf(" ; ");
			continue;
		default:
			printf("Unknown Symbol!!\n");
			return;
		}
	}
	END_FOR:

	printf("-- Parser : \n");

	int identLevel = 0;
	auto parsed = Parse(Scanner(is));
	for (auto symbol : parsed)
	{
		switch (symbol.type)
		{
		case SymbolType::Terminal:
			printf("\n ");
			for (int i = 0; i < identLevel; ++i)
				printf("-");
			printf(" ");
			switch (symbol.terminal)
			{
			case Quadriga::Terminal::ERROR:
				printf("Error!!\n");
				return;
			case Quadriga::Terminal::END:
				printf("End\n");
				break;
			case Quadriga::Terminal::TERMINAL:
				printf(" <TERMINAL> ");
				continue;
			case Quadriga::Terminal::IDENTIFICADOR:
				printf(" <IDENTIFICADOR> ");
				continue;
			case Quadriga::Terminal::PLUS:
				printf(" + ");
				continue;
			case Quadriga::Terminal::ASTERISC:
				printf(" * ");
				continue;
			case Quadriga::Terminal::QUESTION:
				printf(" ? ");
				continue;
			case Quadriga::Terminal::ARROBA:
				printf(" @ ");
				continue;
			case Quadriga::Terminal::NUMBER_SYMBOL:
				printf(" # ");
				continue;
			case Quadriga::Terminal::OR:
				printf(" | ");
				continue;
			case Quadriga::Terminal::DASH:
				printf(" - ");
				continue;
			case Quadriga::Terminal::OPEN_PARENTHESIS:
				printf(" ( ");
				continue;
			case Quadriga::Terminal::CLOSE_PARENTHESIS:
				printf(" ) ");
				continue;
			case Quadriga::Terminal::ASSIGN:
				printf(" = ");
				continue;
			case Quadriga::Terminal::SEMICOLON:
				printf(" ; ");
				continue;
			default:
				printf("Unknown Terminal!!\n");
				return;
			}
			break;
		case SymbolType::NoTerminal:
			printf("\n ");
			for (int i = 0; i < identLevel; ++i)
				printf("-");
			printf(" ");
			switch (symbol.noTerminal)
			{
			case NoTerminal::Error:
				printf("Error!!\n");
				return;
			case NoTerminal::TerminalGroup:
				printf("TerminalGroup");
				break;
			case NoTerminal::ConcatenationExp:
				printf("ConcatenationExp");
				break;
			case NoTerminal::ParenthesisExp:
				printf("ParenthesisExp");
				break;
			case NoTerminal::MultiplierExp:
				printf("MultiplierExp");
				break;
			case NoTerminal::Exp:
				printf("Exp");
				break;
			case NoTerminal::Def:
				printf("Def");
				break;
			case NoTerminal::DefList:
				printf("DefList");
				break;
			}
			++identLevel;
			printf("\n ");
			for (int i = 0; i < identLevel; ++i)
				printf("-");
			printf("[");
			continue;
		case SymbolType::NoTerminalEnd:
			printf("\n ");
			for (int i = 0; i < identLevel; ++i)
				printf("-");
			printf("]");
			--identLevel;
			continue;
		default:
			printf("Unknown NoTerminal!!\n");
			return;
		}
	}

	printf("-- Parser Tree : \n");

	Parser parser;
	parser.ParseDefList(scanner);
}

std::string ToUTF8(const std::u32string s32)
{
	std::string result;
	for (char32_t c : s32)
	{
		if (c < 0x80)
		{
			result += (char)c;
		}
		else if (c < 0x800)
		{
			uint8_t b1 = (c >> 6) & 0x1F;
			uint8_t b2 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xC0);
			result += (char)(b2 & 0x80);
		}
		else if (c < 0x82080)
		{
			uint8_t b1 = (c >> 12) & 0x0F;
			uint8_t b2 = (c >> 6) & 0x3F;
			uint8_t b3 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xE0);
			result += (char)(b2 & 0x80);
			result += (char)(b3 & 0x80);
		}
		else
		{
			uint8_t b1 = (c >> 18) & 0x07;
			uint8_t b2 = (c >> 12) & 0x3F;
			uint8_t b3 = (c >> 6) & 0x3F;
			uint8_t b4 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xF0);
			result += (char)(b2 & 0x80);
			result += (char)(b3 & 0x80);
			result += (char)(b4 & 0x80);
		}
	}
	return result;
}

std::string ToUTF32(const std::string s32)
{
	std::string result;
	for (char32_t c : s32)
	{
		if (c < 0x80)
		{
			result += (char)c;
		}
		else if (c < 0x800)
		{
			uint8_t b1 = (c >> 6) & 0x1F;
			uint8_t b2 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xC0);
			result += (char)(b2 & 0x80);
		}
		else if (c < 0x82080)
		{
			uint8_t b1 = (c >> 12) & 0x0F;
			uint8_t b2 = (c >> 6) & 0x3F;
			uint8_t b3 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xE0);
			result += (char)(b2 & 0x80);
			result += (char)(b3 & 0x80);
		}
		else
		{
			uint8_t b1 = (c >> 18) & 0x07;
			uint8_t b2 = (c >> 12) & 0x3F;
			uint8_t b3 = (c >> 6) & 0x3F;
			uint8_t b4 = (c >> 0) & 0x3F;

			result += (char)(b1 & 0xF0);
			result += (char)(b2 & 0x80);
			result += (char)(b3 & 0x80);
			result += (char)(b4 & 0x80);
		}
	}
	return result;
}

void PrintScanner(const char32_t *is)
{
	using namespace Quadriga;
	Scanner scanner(is);
	Parser parser;
	parser.ParseDefList(scanner);

	std::u32string result;

	result += ToCode(parser.finalAutomat, parser.labels);

	std::ofstream file;
	
	file.open("out.txt", std::ofstream::out);

	file << ToUTF8(result).c_str();

	file.close();

	//printf("%s\n", ToUTF8(result).c_str());
}

int main()
{
	/*printf("hello world\n");

	PrintInputStream(Quadriga::StringInputStream(U"hello world\n"));


	PrintSymbolSequence(Quadriga::StringInputStream(U"('a'-'z')+ | ('0'-'9')*"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"hola = ('a'-'z')+ | ('0'-'9')*;"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"helo = ('a'-'z')+ ('0'-'9' | 'a'-'z')*;  \n  lola = '-';"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"halo = 'h' 'a' 'l' 'o'; \n   helo = ('a'-'z')+ ('0'-'9' | 'a'-'z')*;  \n  lola = '-';"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"halo = 'h' 'a' 'l' 'o'; \n   helo = @ (# | @)*;  \n  lola = '-';"));
*/
	//PrintScanner(U"halo = 'h' 'a' 'l' 'o'; \n   helo = @ (# | @)*;  \n  lola = '-'; QUADRIGA_IGNORE = (' '|'\\t'|'\\n'|'\\r')+; QUADRIGA_BEGIN_COMMENT = ('/' '/') | '#';");
	//ADD_SHADER, ADD_PROGRAM, ADD_TEXTURE, ADD_SPRITE, ADD_OBJ, ADD_FONT, ADD_SUBMESH_GROUP, ADD_MATERIAL

	//*
	PrintScanner(
		U"ADD_SHADER = 'A' 'd' 'd' 'S' 'h' 'a' 'd' 'e' 'r';"
		U"ADD_PROGRAM = 'A' 'd' 'd' 'P' 'r' 'o' 'g' 'r' 'a' 'm';"
		U"ADD_TEXTURE = 'A' 'd' 'd' 'T' 'e' 'x' 't' 'u' 'r' 'e';"
		U"ADD_SPRITE = 'A' 'd' 'd' 'S' 'p' 'r' 'i' 't' 'e';"
		U"ADD_OBJ = 'A' 'd' 'd' 'O' 'B' 'J';"
		U"ADD_OPEN_GEX = 'A' 'd' 'd' 'O' 'p' 'e' 'n' 'G' 'E' 'X';"
		U"ADD_FONT = 'A' 'd' 'd' 'F' 'o' 'n' 't';"
		U"ADD_SUBMESH_GROUP = 'A' 'd' 'd' 'S' 'u' 'b' 'm' 'e' 's' 'h' 'G' 'r' 'o' 'u' 'p';"
		U"ADD_MATERIAL = 'A' 'd' 'd' 'M' 'a' 't' 'e' 'r' 'i' 'a' 'l';"
		U"CREATE_TEXTURE = 'C' 'r' 'e' 'a' 't' 'e' 'T' 'e' 'x' 't' 'u' 'r' 'e';"
		U"COMPILE_SHADERS_AND_LINK_PROGRAMS = 'C' 'o' 'm' 'p' 'i' 'l' 'e' 'S' 'h' 'a' 'd' 'e' 'r' 's' 'A' 'n' 'd' 'L' 'i' 'n' 'k' 'P' 'r' 'o' 'g' 'r' 'a' 'm' 's';"

		U"VERTEX = 'V' 'E' 'R' 'T' 'E' 'X';"
		U"FRAGMENT = 'F' 'R' 'A' 'G' 'M' 'E' 'N' 'T';"

		U"name = 'n' 'a' 'm' 'e';"
		U"type = 't' 'y' 'p' 'e';"
		U"file = 'f' 'i' 'l' 'e';"

		U"color = 'c' 'o' 'l' 'o' 'r';"
		U"albedo = 'a' 'l' 'b' 'e' 'd' 'o';"
		U"normal = 'n' 'o' 'r' 'm' 'a' 'l';"
		U"depth = 'd' 'e' 'p' 't' 'h';"
		U"ambient_occlusion = 'a' 'm' 'b' 'i' 'e' 'n' 't' '_' 'o' 'c' 'c' 'l' 'u' 's' 'i' 'o' 'n';"
		U"specular = 's' 'p' 'e' 'c' 'u' 'l' 'a' 'r';"
		U"roughness = 'r' 'o' 'u' 'g' 'h' 'n' 'e' 's' 's';"
		U"metallic = 'm' 'e' 't' 'a' 'l' 'l' 'i' 'c';"

		U"size_min = 's' 'i' 'z' 'e' '_' 'm' 'i' 'n';"
		U"size_max = 's' 'i' 'z' 'e' '_' 'm' 'a' 'x';"
		U"uv_min = 'u' 'v' '_' 'm' 'i' 'n';"
		U"uv_max = 'u' 'v' '_' 'm' 'a' 'x';"

		U"obj = 'o' 'b' 'j';"
		U"set_material = 's' 'e' 't' '_' 'm' 'a' 't' 'e' 'r' 'i' 'a' 'l';"
		U"add_sub_obj = 'a' 'd' 'd' '_' 's' 'u' 'b' '_' 'o' 'b' 'j';"

		U"SLASH = '/';"
		U"COLON = ':';"
		U"SEMICOLON = ';';"
		U"OPEN_BRACKET = '[';"
		U"CLOSE_BRACKET = ']';"
		U"OPEN_PARENTHESIS = '(';"
		U"CLOSE_PARENTHESIS = ')';"
		U"OPEN_CURLY = '{';"
		U"CLOSE_CURLY = '}';"
		//U"IDENTIFIER = (@ | '_') (# | @ | '_')*;"
		U"INTEGER = ('+' | '-')? ((('1'-'9') (#*)) | '0');"
		U"FLOAT= ('+' | '-')? (#*) '.' (#+);"
		U"STRING = '\"' ((# | @ | ' ' | '!' | '#' - '/' | ':' - '@' | '[' | ']' - '`' | '{' - '~' | ('\\\\' '\"') | ('\\\\' '\\\\') )*) '\"'; " // /*| U+00A0 - U+00BF | U+00D7 | U+00F7* /
		U"QUADRIGA_IGNORE = (' '|'\\t'|'\\n'|'\\r')+;"
		U"QUADRIGA_BEGIN_COMMENT = ('/' '/') | '#';");

		/*/

	PrintScanner(
		U"OPEN_BRACKET = '[';"
		U"CLOSE_BRACKET = ']';"
		U"OPEN_PARENTHESIS = '(';"
		U"CLOSE_PARENTHESIS = ')';"
		U"OPEN_CURLY = '{';"
		U"CLOSE_CURLY = '}';"
		U"COMMA = ',';"
		U"EQUALS = '=';"
		U"PLUS = '+';"
		U"MINUS = '-';"

		// literals
		U"NULL = 'n' 'u' 'l' 'l';"
		U"TRUE = 't' 'r' 'u' 'e';"
		U"FALSE = 'f' 'a' 'l' 's' 'e';"

		U"DECIMAL_LITERAL = # ('_' | #)*;"
		U"HEX_LITERAL = '0' ('X' | 'x') (# | 'a'-'f' | 'A'-'F') ('_' | # | 'a'-'f' | 'A'-'F' )*;"
		U"OCTAL_LITERAL = '0' ('O' | 'o') ('0'-'7') ('_' | '0'-'7' )*;"
		U"BINARY_LITERAL = '0' ('B' | 'b') ('0' | '1' ) ('_' | '0' | '1')*;"
		U"CHAR_LITERAL = '\\\'' ( '\\u20' | '\\u21' | '\\u23'-'\\u5b' | '\\u5d'-'\\u7e' | '\\ua0'-'\\ud7ff' | '\\ue000'-'\\ufffd' | '\\u10000'-'\\u10ffff' | ( '\\\\' ( '\\\'' | '\\\"'  | '?'   | '\\\\'   | 'a'   | 'b'   | 'f'   | 'n'   | 'r'   | 't'   | 'v'   | ('x' ('_' | # | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' ) ('_' | # | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' ) ) ) ) ) '\'';"
		
		U"FLOAT_LITERAL = ((# ( # | '_' )*)?  '.' (# ( # | '_' )*)? ( ('E' |'e') ('+' | '-')? (# ( # | '_' )* )? )?) | ((# ( # | '_' )*)? ('E' |'e') ('+' | '-')? (# ( # | '_' )* )?);"
		// TODO STRING_LITERAL
		U"STRING_LITERAL = '\\\"' ( "
				U"'\\u20'-'\\u26' | '\\u28'-'\\u5b' | '\\u5d'-'\\u7e' "
				U"| ( '\\\\' ( '\\\'' | '\\\"' | '?' | '\\\\' | 'a' | 'b' | 'f' | 'n' | 'r  | 't' | 'v' "
					U" | ('x' ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) )"
					U" | ('u' ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) )"
					U" | ('U' ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) ( # | 'a'-'f' | 'A'-'F' ) )"
					U" ) )"
				U" ) '\"';"


		// data types
		U"BOOL = ('b' 'o' 'o' 'l') | ('b');"
		U"INT8  = ('i' 'n' 't' '8') | ('i' '8');"
		U"INT16 = ('i' 'n' 't' '1' '6') | ('i' '1' '6');"
		U"INT32 = ('i' 'n' 't' '3' '2') | ('i' '3' '2');"
		U"INT64 = ('i' 'n' 't' '6' '4') | ('i' '6' '4');"
		U"UNSIGNED_INT8  = ('u' 'n' 's' 'i' 'g' 'n' 'e' 'd' '_' 'i' 'n' 't' '8') | ('u' '8');"
		U"UNSIGNED_INT16 = ('u' 'n' 's' 'i' 'g' 'n' 'e' 'd' '_' 'i' 'n' 't' '1' '6') | ('u' '1' '6');"
		U"UNSIGNED_INT32 = ('u' 'n' 's' 'i' 'g' 'n' 'e' 'd' '_' 'i' 'n' 't' '3' '2') | ('u' '3' '2');"
		U"UNSIGNED_INT64 = ('u' 'n' 's' 'i' 'g' 'n' 'e' 'd' '_' 'i' 'n' 't' '6' '4') | ('u' '6' '4');"
		U"FLOAT16 = ('f' 'l' 'o' 'a' 't' '1' '6') | ('h') | ('f' '1' '6');"
		U"FLOAT32 = ('f' 'l' 'o' 'a' 't' '3' '2') | ('f') | ('f' '3' '2');"
		U"FLOAT64 = ('f' 'l' 'o' 'a' 't' '6' '4') | ('d') | ('f' '6' '4');"
		U"STRING = ('s' 't' 'r' 'i' 'n' 'g') | ('s');"
		U"REF = ('r' 'e' 'f') | ('r');"
		U"TYPE = ('t' 'y' 'p' 'e') | ('t');"

		// identifier
		U"IDENTIFIER = (@ | '_') (@ | # | '_')*;"
		U"NAME = ('%' | '$' ) (@ | '_') (@ | # | '_')*;"

		// OTHER
		U"QUADRIGA_IGNORE = (' '|'\\t'|'\\n'|'\\r')+;"
		U"QUADRIGA_BEGIN_COMMENT = ('/' '/') | '#';"
	);

	*/
/*

	PrintSymbolSequence(Quadriga::StringInputStream(U"hello world 12345 | = asd < wert >\n"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"hello world12345|=asd<wert>\n123>><|(asdf|32)"));
	PrintSymbolSequence(Quadriga::StringInputStream(U"hello world12345|=asd<wert>\n123>//><|(asdf|32)\n"));*/
	//PrintSymbolSequence(Quadriga::StringInputStream(U"hello world /* 12345|=asd<wert> \n 1 */ 23> //><|(asdf|32)\n"));	
	//PrintSymbolSequence(Quadriga::StringInputStream(U"hello world /* 12345|=asd /*<wert*/ > \n 1 */ 23> //><|(asdf|32)\n"));

    return 0;
}


namespace Test
{
	
}